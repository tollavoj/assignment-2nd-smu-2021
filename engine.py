import itertools
import pycosat
import time
from collections import defaultdict
from typing import List
from logic import *

'''
This file contains subsumption engine (based on pycosat).
'''


def addSpecialSymbol(literal: Literal) -> Literal:
    prefix = "~~" if literal.positive else "~~!"
    predicate = Predicate(prefix + literal.atom.predicate.name, literal.atom.predicate.arity)
    return Literal(Atom(predicate, literal.atom.terms), positive=True)


def addExactlyOneHoldConstraint(constraints: List[List[int]], variables: Iterable[int]) -> None:
    constraints.append([variable for variable in variables])
    for i, j in itertools.combinations(variables, r=2):
        constraints.append([-i, -j])


def findPycosatSubstitutions(allSolutions: bool, query: Iterable[Literal], world: Iterable[Literal]) -> List[Dict[Variable, Term]]:
    # general case
    start = time.time()
    # encode to CSP

    # encode instance
    literalsPerPredicate: Dict[Predicate, Set[Literal]] = defaultdict(set)
    totalVars = 0

    terms: Iterable[Term] = unionSets(set(literal.atom.terms) for literal in world)
    universalDomain: Set[Variable] = set()

    def varCounter():
        nonlocal totalVars
        totalVars += 1
        return totalVars

    assignments: Dict['var', Dict['term', int]] = defaultdict(lambda: defaultdict(varCounter))  # id of var->term, i.e. assignment[var][term] returns id of propositional variable
    for literal in world:
        literalsPerPredicate[literal.getPredicate()].add(literal)
        if not literal.positive:
            raise ValueError("Only non-negated literals are allowed for the world variable.")

    constraints: List[List[int]] = []
    for literal in query:
        if literal.isGround():
            if (literal.positive and literal not in world) or ((not literal.positive) and literal.negation() in world):
                return []  # if a query's l is ground but is not in the world, then the query cannot subsume the world (similarly for negative literal)
            continue

        if not literal.positive:
            for variable in literal.getVariables():
                universalDomain.add(variable)

        possibleMappings: Set[Tuple[int]] = set()
        for potential in literalsPerPredicate[literal.getPredicate()]:
            possibleUnification = unifyFirstWithSecond(literal, potential)
            if None is possibleUnification:
                continue
            possibleMappings.add(tuple(assignments[var][target] for var, target in possibleUnification.items()))

        if literal.positive and not possibleMappings:  # there is no literal in world to which l can be mapped, therefore unsatisfiable
            return []

        if literal.positive:
            mappingVariants = []
            for mapping in possibleMappings:
                auxiliarLiteralVariable = varCounter()
                mappingVariants.append(auxiliarLiteralVariable)
                implication = [-propositionalVariable for propositionalVariable in mapping]
                implication.append(auxiliarLiteralVariable)
                constraints.append(implication)
                for propositionalVariable in mapping:
                    constraints.append([propositionalVariable, -auxiliarLiteralVariable])
            addExactlyOneHoldConstraint(constraints, mappingVariants)
        else:  # actually restriction on solutions
            for forbidden in possibleMappings:
                constraints.append([-propositionalVariable for propositionalVariable in forbidden])

    for variable, term in itertools.product(universalDomain, terms):
        addingPropositionalVariable = assignments[variable][term]

    for var, termsAssignment in assignments.items():
        addExactlyOneHoldConstraint(constraints, termsAssignment.values())

    encodingTime = time.time() - start
    # solver
    start = time.time()
    solutions = []
    if allSolutions:
        for solution in pycosat.itersolve(constraints):
            if solution not in ("UNSAT", "UNKNOWN"):
                solutions.append(solution)
    else:
        solution = pycosat.solve(constraints)
        if solution not in ("UNSAT", "UNKNOWN"):
            solutions.append(solution)

    satTime = time.time() - start
    # print("encoding\t{}\nsat     \t{}".format(encodingTime, satTime))
    # transfer solution to FOL representation
    propositionalToAssignment = {propositionalVariable: (variable, term) for variable, termsAssignment in assignments.items() for term, propositionalVariable in termsAssignment.items()}
    substitutions = [dict(propositionalToAssignment[propositionalVariable] for propositionalVariable in model if propositionalVariable in propositionalToAssignment) for model in solutions]
    return substitutions


def findSubstitutions(query: Clause, world: Clause, allSolutions=True) -> List[Dict[Variable, Term]]:
    '''
    By default it searches for all substitutions (all_solutions=True). If all_solutions=False, then only one is returned.

    :param query: Clause
    :param world: Clause
    :param allSolutions: bool 
    :return: List[Dict[Variable,Term]]
    '''
    query = Clause(map(addSpecialSymbol, query))
    world = Clause(map(addSpecialSymbol, world))

    # special (ground) case
    if len(query.getVariables()) < 1:
        for l in query:
            if (l.positive and l not in world) or ((not l.positive) and l.negation() in world):
                return []
        return [{}]  # although it looks weird, it actually means that there is one empty substitution, i.e. a very trivial one

    return findPycosatSubstitutions(allSolutions, query, world)


def subsumes(query: Clause, world: Clause) -> bool:
    '''
    Given a world and a query, the method decides whether the query theta-subsume the world.
    :param query: Clause 
    :param world: Clause
    :return: bool
    '''
    substitutions = findSubstitutions(query, world, allSolutions=False)
    return len(substitutions) > 0

